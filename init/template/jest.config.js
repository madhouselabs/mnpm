module.exports = {
    setupFilesAfterEnv: ['./.jest/setup.js'],
    transform: {
        '\\.[jt]sx?$': 'babel-jest',
    },
    moduleDirectories: ['node_modules', 'packages'],
};
