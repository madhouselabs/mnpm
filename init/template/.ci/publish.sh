#!/usr/bin/env bash
echo $GITLAB_NPM_REGISTRY_TOKEN
CWD=$(pwd)
for DIFF_PATH in $(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA | grep package.json)
  do
    pnpx jest "$(dirname $DIFF_PATH)"
    cd $(dirname $DIFF_PATH)
    cp $CWD/.ci/.npmrc .npmrc
    cp $CWD/.ci/setup-package.js setup-package.js
    npx babel-cli --ignore '**/*.test.js' --root-mode=upward ./src -d dist
    node setup-package.js $CI_PROJECT_ID

    npx pnpm i

    echo "**" >> .npmignore
    echo "!package.json" >> .npmignore
    echo "!pnpm-lock.yaml" >> .npmignore
    echo "!dist/**" >> .npmignore

    cat ./package.json
    npm publish
  done
