const pack = require("./package.json");
const path = require("path");
const fs = require("fs");

pack.main = "dist/index.js"

pack.publishConfig =  {
  "@madhouselabs:registry": `https://gitlab.com/api/v4/projects/${process.argv[2]}/packages/npm/`
};

fs.writeFileSync(path.resolve(__dirname,"./package.json"),JSON.stringify(pack));


