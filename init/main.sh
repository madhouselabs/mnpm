CWD=$(pwd)
DIR=$(dirname $0)
mkdir packages

cp -r $DIR/template/{.ci,.gitlab-ci.yml,.jest,.eslintrc.yml,.gitignore,babel.config.js,jest.config.js,package.json,pnpm-workspace.yaml} $CWD

pnpm i @babel/core @babel/cli @babel/preset-env babel-jest babel-polyfill esm jest -D -W
git init
npx install-peerdeps -P -D @madhouselabs/eslint-config-base --extra -W

rm .git/hooks/pre-commit*
cp $DIR/.githooks/pre-commit .git/hooks/pre-commit
cd $CWD
