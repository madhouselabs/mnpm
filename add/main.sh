CWD=$(pwd)
MOD_DIR=$CWD/packages/$1

cd $MOD_DIR
shift 1
pnpm i $@

cd $CWD
