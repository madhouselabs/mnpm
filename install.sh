#! /usr/bin/env sh

DIR=$HOME/.local/bin
[ -d $DIR ] || mkdir -p $DIR

ORG_DIR=$(pwd)

EXEC_PATH=$DIR/mnpm

if [ -d $EXEC_PATH ]
then
  cd $EXEC_PATH
  git pull
  cd $ORG_DIR
else
  echo 'export PATH=$PATH:$HOME/.local/bin/mnpm' >> "${ZDOTDIR:-$HOME}"/.zshrc
  git clone https://gitlab.com/madhouselabs/mnpm $EXEC_PATH
fi


