const packagePath = "./package.json";
const pack = require(packagePath);
const fs = require("fs");
const path = require("path");
pack.main = "src/index.js";

fs.writeFileSync(path.resolve(__dirname, "package.json"), JSON.stringify(pack,null, 2));
