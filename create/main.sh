CWD=$(pwd)
DIR=$(dirname $0)

MOD_DIR=$CWD/packages/$1
SCOPE_NAME=$(echo $1 | awk -F/ '{print $1}')

mkdir -p $MOD_DIR/src/__tests__
cd $MOD_DIR
if [ $SCOPE_NAME = $1 ]
then
  pnpm init -y
else
  pnpm init -y --scope=$SCOPE_NAME
fi
cp $DIR/cleanup-package.js .
node cleanup-package.js
rm cleanup-package.js
cd $CWD



cp $DIR/index.js $MOD_DIR/src/
cp $DIR/index.test.js $MOD_DIR/src/__tests__

