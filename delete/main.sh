#! /usr/bin/env sh

CWD=$(pwd)
MOD_DIR=$CWD/packages/$1
cd $MOD_DIR
shift 1
pnpm uninstall $@
cd $CWD