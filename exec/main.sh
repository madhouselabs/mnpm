#! /usr/bin/env zsh

CWD=$(pwd)

for dir in $(ls $PWD/apps/**/package.json)
do
    cd $(dirname $dir)
    eval $@
done
cd $CWD
