module.exports = {
  setupFilesAfterEnv: ['setup-tests.js'],
  transform: {
    '.*[.]jsx?$': 'babel-jest',
  },
};
