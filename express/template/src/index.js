import { useHttpLogging } from '@madhouselabs/http-helpers';
import './db';
import { createExpressApp, useErrorHandler } from './app';

const app = createExpressApp();

useHttpLogging(app);

useErrorHandler(app);

app.listen(Number(process.env.PORT), () => {
  Logger.info(`Apps repo started over port ${process.env.PORT}`);
});
