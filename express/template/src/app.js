import express from 'express';
import { StatusCodes } from 'http-status-codes';
import createError from 'http-errors-lite';

export const createExpressApp = () => {
  const app = express();
  app.get('/healthy', (req, res) => {
    res.send(StatusCodes.OK);
  });
  app.use(express.json());
  return app;
};

const notFoundHandler = (req, res, next) => {
  next(createError(StatusCodes.NOT_FOUND, `${req.originalUrl} is  not found.`));
};

const errorHandler = (err, req, res, _next) => {
  res
    .status(err.statusCode || 500)
    .send(err.message || 'Something bad happened');
};

export const useErrorHandler = (app) => {
  app.use(notFoundHandler);
  app.use(errorHandler);
};