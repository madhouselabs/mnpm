import { initZConfig } from '@madhouselabs/ccm';

(async () => {
  initZConfig(
    {
      cors_regex: '[.]localhost.com$',
    },
    process.env.NODE_ENV === 'development'
  );
})();
