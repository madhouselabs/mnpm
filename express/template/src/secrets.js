import { initZSecret } from '@madhouselabs/csm';

(async () => {
  initZSecret(
    {
      db_url: 'mongodb://localhost:27017/local-db',
    },
    process.env.NODE_ENV === 'development'
  );
})();
