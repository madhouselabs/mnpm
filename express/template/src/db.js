import secrets from '@madhouselabs/csm';
import mongoose from 'mongoose';

(async () => {
  await secrets.wait();
  try {
    await mongoose.connect(secrets.db_url, {
      useFindAndModify: false,
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    Logger.info('DB connected successful');
  } catch (err) {
    Logger.error('Failed to connect to DB ');
  }
})();
