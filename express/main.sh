#! /usr/bin/env sh

DIR=$(dirname $0)

## Sanity Check
[ $(ls | wc -l) -ne 0 ] && echo "Directory is not empty, dropping ...." && exit 17;

cp -r $DIR/template/* $PWD/
cp -r $DIR/template/* $PWD/

pnpm i